

package myworkflow;
//-------------------------------------------Importing in built libraries-------------------------//
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.io.File;
import java.util.*;

//-----------------------------------------------Importing third party libraries ---------------------//
import com.rabbitmq.client.*;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.ParseException;
import org.json.simple.parser.JSONParser;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;



public class MyWorkflow {
    
    public void generateAndSendMessage(String serviceName,String currentStepType,Object[] arr,Channel WorkflowFactorychannel, String LB_QUEUE) throws UnsupportedEncodingException, IOException
    {
        JSONObject obj = new JSONObject();
        obj.put(currentStepType+"_name", serviceName);
        obj.put("params", arr);
        WorkflowFactorychannel.basicPublish("", LB_QUEUE, null, obj.toString().getBytes("UTF-8"));
        return;
    }
    
    public Object[] recieveReply(Channel ResultManagerChannel,String resultQueueName)
    {
        
    } 
    public Object[] executeStep(Object[] arr,int i,Channel ResultManagerChannel,String resultQueueName, Channel WorkflowFactorychannel, String WorkflowFactoryQueueName,NodeList stepsList) throws IOException
    {
        MyWorkflow mywrkflw = new MyWorkflow();
        String currentStepType = ((Element)stepsList.item(i)).getElementsByTagName("type").item(0).getTextContent().toString();
        if( !currentStepType.equals("IF") )
        {
            String serviceName = ((Element)stepsList.item(i)).getElementsByTagName("name").item(0).getTextContent().toString();
            mywrkflw.generateAndSendMessage(serviceName,currentStepType,arr,WorkflowFactorychannel,WorkflowFactoryQueueName);
            String returnType = ((Element)stepsList.item(i)).getElementsByTagName("returnType").item(0).getTextContent().toString();
            if(!returnType.equals("void"))
            {
                Object[] result = mywrkflw.recieveReply(ResultManagerChannel,resultQueueName); 
            }
        }
        return null;
    }       
    
    public void workflowExecution(String message,Channel ResultManagerChannel,String resultQueueName,Channel WorkflowFactorychannel,String WorkflowFactoryQueueName,NodeList stepsList) throws ParseException, IOException
    {
        MyWorkflow mywrkflw = new MyWorkflow();
        String input = message; //here first extract the input part
        JSONParser parser = new JSONParser();
        JSONObject jo = null;
        jo = (JSONObject) parser.parse(message);
        Object[] arr = (Object[])jo.get("params");
        
        for(int i = 0; i < stepsList.getLength() - 1 ; )
        {
            //Execute Step function will return two things namely output of step , its datatype and next i value
            mywrkflw.executeStep(arr,i,ResultManagerChannel,resultQueueName,WorkflowFactorychannel,WorkflowFactoryQueueName,stepsList);
            uPDATE i value
        }
    }
    
    public void workflowEngine(String[] args) throws IOException, TimeoutException, ParserConfigurationException, SAXException 
    {
        //first declaring queue for receiving request from load balancer
        InetAddress inetAddress = InetAddress.getLocalHost();
        ConnectionFactory LBconnectionFactory = new ConnectionFactory();
        LBconnectionFactory.setUsername("ias");
        LBconnectionFactory.setPassword("ias");
        LBconnectionFactory.setVirtualHost("/");
        LBconnectionFactory.setHost("localhost");
        LBconnectionFactory.setPort(5672);
        Connection connection = (Connection) LBconnectionFactory.newConnection();
        final Channel channel = ((com.rabbitmq.client.Connection) connection).createChannel();
        String workflowQueueName = args[0]; //dockerid as queue name
        channel.queueDeclare(workflowQueueName, false, false, false, null);
        System.out.println("creating LB workflow queue name " + workflowQueueName);
        
        //Now creating queue for intermediate communication i.e. waiting for getting results of intermedate steps 
        ConnectionFactory ResultManagerCF = new ConnectionFactory();
        ResultManagerCF.setUsername("ias");
        ResultManagerCF.setPassword("ias");
        ResultManagerCF.setVirtualHost("/");
        ResultManagerCF.setHost("localhost");
        ResultManagerCF.setPort(5672);
        Connection ResultManagerConn = (Connection) ResultManagerCF.newConnection();
        final Channel ResultManagerChannel = ((com.rabbitmq.client.Connection) ResultManagerConn).createChannel();
        String resultQueueName = args[0]+"_ResultQueueName";
        ResultManagerChannel.queueDeclare(resultQueueName, false, false, false, null);
        System.out.println("creating result workflow queue name " + workflowQueueName);
        
        //Creating queue for sending service requests to load balancer
        ConnectionFactory WorkflowFactory = new ConnectionFactory();
        WorkflowFactory.setUsername("ias");
        WorkflowFactory.setPassword("ias");
        WorkflowFactory.setVirtualHost("/");
        WorkflowFactory.setHost("localhost");
        WorkflowFactory.setPort(5672);
        Connection WorkflowFactoryconnection = (Connection) WorkflowFactory.newConnection();
        final Channel WorkflowFactorychannel = ((com.rabbitmq.client.Connection) WorkflowFactoryconnection).createChannel();
        String WorkflowFactoryQueueName = "lb_incomming"; //load balancer's ip queue name
        WorkflowFactorychannel.queueDeclare(WorkflowFactoryQueueName, false, false, false, null);
        
        MyWorkflow mywrkflw = new MyWorkflow();
        //----------------For reading xml files------------
        String filePath = args[1];
        File xmlFile = new File(filePath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(xmlFile);
        doc.getDocumentElement().normalize();
        NodeList stepsList = doc.getElementsByTagName("step");
        
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
        public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
            throws IOException {
          String message = new String(body, "UTF-8");
                try {
                    //System.out.println( message WorkflowFactorychannel);
                    /*--------------------------Calling function that will begin execution of workflow-----------*/
                    //First parameter is the message received
                    //Second parameter is the Channel object that for the queue that will receive intermediate result
                    //Third parameter is the queuename of the ResultManagerChannel
                    //Fourth parameter is the WorkflowFactorychannel that is used for sending request to loadbalancer
                    //Fifth parameter is corresponding queue name
                    // List of Nodes
                    mywrkflw.workflowExecution(message,ResultManagerChannel,resultQueueName,WorkflowFactorychannel,WorkflowFactoryQueueName,stepsList);
                } catch (ParseException ex) {
                    Logger.getLogger(MyWorkflow.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        channel.basicConsume(workflowQueueName, true, consumer);
    }
    
    public static void main(String[] args) {
        MyWorkflow mywrkflw = new MyWorkflow();
        try 
        {
            mywrkflw.workflowEngine(args);
        }
        catch(Exception e)
        {
            System.out.println("execption "+e);
        }
    }
}

