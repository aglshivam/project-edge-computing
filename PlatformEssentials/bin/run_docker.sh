# build the docker image and give the image name arg passed at run time
echo $1
sudo docker build -t $1 .

# run the docker image passing the argument to the docker file using var1 agrument
sudo docker run  -it -e var1=$1 --net=host $1 
