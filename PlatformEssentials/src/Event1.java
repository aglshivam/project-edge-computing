/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.*;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Ayushi
 */


public class Event1 extends Event {

    /**
     * @param args the command line arguments
     */
    String msg="";
    
    public void event1(){
    	try
    	{

    		Event e = new Event();
    		String result1 = e.fetch_rule_data("event1rule1");
    		String result2 = e.fetch_rule_data("event1rule2");
    	    System.out.println("result1="+result1);
	        System.out.println("result2="+result2);
	        if((result1.equals("true"))&&((result2.equals("false")))){             
               System.out.println("in event1 if");
               //call_service("foo","in foo");
	        }
           else
           {
        	   System.out.println("in event1 else");
        	   //call_service("fun");
           }    
         call_service("foo","in foo");
	       call_service("fun","in funny :)");
	        	}
    	catch(Exception e)
    	{
    		System.out.println(e);
    	}
   }
   public void event2() 
   {
	   try 
	   {
//		 if((fetch_rule_data("event2rule1")=="false")&&((fetch_rule_data("event2rule4")=="true"))||((fetch_rule_data("event2rule5")=="false")&&((fetch_rule_data("event2rule9")=="false"))))
//		 {
//		    
//		    System.out.println("in if of event 2 ");
//		    call_service("fun");
//		 }
//		 else
//		 {
//			 System.out.println("in else of event2");
//			 call_service("foo",6,"in foo");
//		 }
		   System.out.println("In event 2");
		 call_service("fun", null);
		 	System.out.println("After calling fun");
		 call_service("bar", null);
		 	System.out.println("After calling bar");
		 
	   }
	   catch(Exception e)
	   {
		   System.out.println(e);
	   }
  }
//   public void event3(){
//	   
//   }
    
    
    
}
