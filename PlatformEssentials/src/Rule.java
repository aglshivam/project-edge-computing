import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;

import java.io.File;
import java.io.IOException;
import java.util.Vector;
import java.util.concurrent.TimeoutException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.rabbitmq.client.Channel;


public class Rule {
	
	private static ConnectionFactory factory ;
	private static Connection connection ;
	private static Channel channel ;
	
	public static Vector<String> rules;
	
	public void init() throws IOException, TimeoutException
	{
		//System.out.println("rule queues");
		//read_rules("rules.xml");
		 factory = new ConnectionFactory();
	 		factory.setUsername("ias");
	 		factory.setPassword("ias");
	         factory.setVirtualHost("/");
	 		factory.setHost("localhost");
	 		factory.setPort(5672);
	 		connection = factory.newConnection();
	 		channel = connection.createChannel();
	}

	
	public void putRuleData(String rule_name,String data) throws IOException
	{
	     try
	     {
	 		//System.out.println("Rule-main");
	    	 channel.exchangeDeclare(rule_name,"fanout");
		channel.basicPublish(rule_name,"",null,data.getBytes());
	     }
	     catch(Exception e)
	     {
	    	 e.printStackTrace();
	    	 
	     }
	}

//	public static void main(String[] args) throws Exception {
//		
//		channel.close();
//		connection.close();
//	}

}
