import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.TimeoutException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

//class CheckServiceAvailable implements Runnable {
//	LoadBalancer lb;
//	String servName;
//
//	public CheckServiceAvailable(LoadBalancer m1, String serviceName) {
//		this.lb = m1;
//		this.servName = serviceName;
//		new Thread(this, "CheckServiceUp").start();
//	}
//
//	public void run() {
//		while(!lb.index.containsKey(servName)) {
//			// Do nothing
//		}
//		lb.serviceSpawned = true;
//	}
//}

class L1 implements Runnable {
   LoadBalancer lb;

   public L1(LoadBalancer m1) {
      this.lb = m1;
      new Thread(this, "FetchPings").start();
   }

   public void run() {
   try {
		lb.processQueueRequests();
	} catch (IOException | TimeoutException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
   }
}

class L2 implements Runnable {
   LoadBalancer lb;

   public L2(LoadBalancer m2) {
      this.lb = m2;
      new Thread(this, "UpdateRoute").start();
   }

   public void run() {
	   try {
			lb.fetchRoutingInformation();
		} catch (IOException | TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
   }
}

class LoadBalancerExec {
	public static void main(String[] args) throws IOException, TimeoutException {
		// TODO: Create a thread for monitoring pings
		
		LoadBalancer lb = new LoadBalancer();
		new L1(lb);
		new L2(lb);
	}
}
