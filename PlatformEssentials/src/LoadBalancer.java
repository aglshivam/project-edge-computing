import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.*;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.ParseException;
import org.json.simple.parser.JSONParser;


public class LoadBalancer {
	
	private final static String REQUEST_QUEUE = "lb_incomming"; // Load balancer incomming request queue
	private final static String LB_SM_QUEUE = "lb_sm_conn"; // Load balancer - service manager queue
	private static final String LB_SERVER_QUEUE = "lb_server_conn";
	
	// This data structure will hold the routing information
	HashMap<String, Vector<String> > routingTable = new HashMap<String, Vector<String> >();
	
	// Keeps track of from which index of docker instance list to use next
	public HashMap<String, Integer> index = new HashMap<String, Integer>();
	
	Vector<String> dockerList = new Vector<String>();
	
	// Shared lock to synchronize the access of routingTable from processQueueRequests and UpdateRoutingInformation
	private static Object sharedLock = new Object();
	
	// Method will continually read the queue and route the requests based on the routing table
	void processQueueRequests() throws IOException, TimeoutException {
		InetAddress inetAddress = InetAddress.getLocalHost();
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setUsername("ias");
        connectionFactory.setPassword("ias");
        connectionFactory.setVirtualHost("/");
        connectionFactory.setHost("10.3.1.59");
        connectionFactory.setPort(5672);
        Connection connection = (Connection) connectionFactory.newConnection();
        final Channel channel = ((com.rabbitmq.client.Connection) connection).createChannel();
        
        ConnectionFactory ServerManagerCF = new ConnectionFactory();
        ServerManagerCF.setUsername("ias");
        ServerManagerCF.setPassword("ias");
        ServerManagerCF.setVirtualHost("/");
        ServerManagerCF.setHost("10.3.1.59");
        ServerManagerCF.setPort(5672);
        Connection ServerManagerConn = (Connection) ServerManagerCF.newConnection();
        final Channel ServerManagerChannel = ((com.rabbitmq.client.Connection) ServerManagerConn).createChannel();
        
        ServerManagerChannel.queueDeclare(LB_SERVER_QUEUE, false, false, false, null);
        channel.queueDeclare(REQUEST_QUEUE, false, false, false, null);
        
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                            throws IOException {
            	
            	String message = new String(body, "UTF-8");
            		
            	JSONParser parser = new JSONParser();
            	JSONObject jo = null;
            	try {
					jo = (JSONObject) parser.parse(message);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				};
				
				String serviceName = (String) jo.get("service_name");
				String dockerInstanceQueue;
				
				Vector<String> dockerInstances;
				
				System.out.println("Recieved request for " + serviceName);
				
				if (!index.containsKey(serviceName)) {
					// Service does not exit, need to spawn a new docker instance, contact server manager
				
					try {
                                                        Thread.sleep(100);
                                                } catch (InterruptedException e) {
                                                        // TODO Auto-generated catch block
                                                        e.printStackTrace();
                                                }

					if (index.containsKey(serviceName)) {
						System.out.println("found key");	
					} else {
	
					System.out.println("Publish request for new spawn" + serviceName);
					
					channel.basicPublish("", LB_SERVER_QUEUE,
                            MessageProperties.PERSISTENT_TEXT_PLAIN,
                            serviceName.getBytes("UTF-8"));
					
					while(!index.containsKey(serviceName)) {
						// wait here until index.containsKey(serviceName) is true
						try {
							Thread.sleep(100);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					System.out.println("Service" + serviceName + "spawned");
					}
				}
				
				// Consult the routing table to check where to forward the request
				synchronized(sharedLock) {
						Integer idx = index.get(serviceName);
						int i = idx.intValue();
						dockerInstances = routingTable.get(serviceName);
					
						// dockerInstanceQueue will be the name of the queue to forward the request to
						dockerInstanceQueue = dockerInstances.get(i);
						
						// Updating the index for next request for this service
						index.put(serviceName, (i + 1) % dockerInstances.size());
				}
				
				System.out.println("Service " + serviceName + " forwarded to " + dockerInstanceQueue);
				
				channel.queueDeclare(dockerInstanceQueue, false, false, false, null);
                channel.basicPublish("", dockerInstanceQueue,
                             MessageProperties.PERSISTENT_TEXT_PLAIN,
                             jo.toString().getBytes("UTF-8"));
				
				
            }
          };
          channel.basicConsume(REQUEST_QUEUE, true, consumer);
	}
	
	// Method will fetch the routing table from the service manager periodically
	void fetchRoutingInformation() throws IOException, TimeoutException {
		InetAddress inetAddress = InetAddress.getLocalHost();
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setUsername("ias");
        connectionFactory.setPassword("ias");
        connectionFactory.setVirtualHost("/");
        connectionFactory.setHost("10.3.1.59");
        connectionFactory.setPort(5672);
        Connection connection = (Connection) connectionFactory.newConnection();
        Channel channel = ((com.rabbitmq.client.Connection) connection).createChannel();
        
        channel.queueDeclare(LB_SM_QUEUE, false, false, false, null);
        
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                            throws IOException {
            		
            	// TODO: Read routing information and construct the routing table
            	
            	String message = new String(body, "UTF-8");
            	
            	JSONParser parser = new JSONParser();
            	JSONObject jo = null;
            	try {
					jo = (JSONObject) parser.parse(message);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	
            	String service_name = (String) jo.get("service_name");
            	
//            	JSON message structure from service manager :
//            	
//            	RoutingInformationUpdate {
//            	    “services_active” : [
//            	        {
//            	            “service_name” : <service-name>,
//            	            “docker_instances” : [
//            	                {
//            	                    “queue_name” : <docker-queue-name>,
//            	                },
//            	                {
//            	                    “queue_name” : <docker-queue-name>,
//            	                },
//            	                …….
//            	                {
//            	                    “queue_name” : <docker-queue-name>,
//            	                },
//            	            ]
//            	        },
//
//            	        …….
//
//            	        {
//            	            “service_name” : <service-name>,
//            	            “docker_instances” : [
//            	                {
//            	                    “queue_name” : <docker-queue-name>,
//            	                },
//            	                {
//            	                    “queue_name” : <docker-queue-name>,
//            	                },
//            	                …….
//            	                {
//            	                    “queue_name” : <docker-queue-name>,
//            	                },
//            	            ]
//            	        },
//            	    ]
//            	}

            	
            	JSONArray activeServices = (JSONArray) jo.get("services_active");
            	
            	String serviceName = null;
            	
            	synchronized(sharedLock) {
            	
	            	// Iterator for active services
	            	for(Object projectObj: activeServices.toArray()) {
	            		JSONObject project = (JSONObject)projectObj;
	            		serviceName = (String) project.get("service_name");
	            		
	            		// Array for the service instances running on separate docker instances
	            		JSONArray serviceInstances = (JSONArray) project.get("docker_instances");
	            		
	            		// Iterator for the queues of each service instance
	            		for(Object projectObj2: serviceInstances.toArray()) {
	            			JSONObject project2 = (JSONObject)projectObj2;
	            			String serviceQueueName = (String) project2.get("queue_name");
	            			dockerList.add(serviceQueueName);
	            		}
	            		routingTable.put(serviceName, dockerList);
	            		index.put(serviceName, 0);
	            	}
            	}
            }
          };
          channel.basicConsume(LB_SM_QUEUE, true, consumer);
	}
}
