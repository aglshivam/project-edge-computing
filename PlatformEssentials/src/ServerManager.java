import java.io.IOException;
import java.net.InetAddress;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.TimeoutException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.MessageProperties;

public class ServerManager {
	
	private static final String LB_SM_QUEUE = "lb_server_conn"; // Load balancer - Server manager queue
	private final static String SERVER_PING_QUEUE = "server_incoming"; // Server manager queue

	private static ConnectionFactory factory;
	private static Connection connection;
	private static Channel channel;
	
	static Map<String,Double> serverInfo = new HashMap<String, Double>();
	static Vector<String> keys = new Vector<String>();
	static int noOfAgents = 0;
	
	//Hash map to check currently running dockers
	static Map<String,Boolean> runningServers = new HashMap<String, Boolean>();
	
	//Hash map for dockers corresponding to ip's
	static Map<String,Vector<String>> dockerForIP = new HashMap<String, Vector<String>>();
	
	//Hash map for service corresponding to docker-id
	static Map<String,String> serviceforDocker = new HashMap<String, String>();
	
	private static String getMinKey()
	{
	    List list = new LinkedList(serverInfo.entrySet());

	    Collections.sort(list, new Comparator()
	    {
	        public int compare(Object o1, Object o2)
	        {
	            return ((Comparable) ((Map.Entry) (o1)).getValue())
	                  .compareTo(((Map.Entry) (o2)).getValue());
	        }
	    });

	    HashMap sortedHashMap = new LinkedHashMap();
	    for (Iterator it = list.iterator(); it.hasNext();)
	    {
	        Map.Entry entry = (Map.Entry) it.next();
	        sortedHashMap.put(entry.getKey(), entry.getValue());
	    } 
	    serverInfo = sortedHashMap;

	    Iterator entries = serverInfo.entrySet().iterator();
		Map.Entry entry = null;
		if(entries.hasNext())
			entry = (Map.Entry) entries.next();
		if(entry==null)
			return null;
	    return (String)entry.getKey();
	}
	
	private static void init() throws IOException, TimeoutException
	{
		factory = new ConnectionFactory();

	    factory.setUsername("ias");
        factory.setPassword("ias");
		
        factory.setVirtualHost("/");
        factory.setHost("10.3.1.59");
        factory.setPort(5672);
        connection = factory.newConnection();
	    channel = connection.createChannel();
	    channel.queueDeclare(SERVER_PING_QUEUE, false, false, false, null);
	}

	// Method will accept request for spawning new services
	void spawnServiceHandler() throws IOException, TimeoutException {
		
		noOfAgents++;
		InetAddress inetAddress = InetAddress.getLocalHost();
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setUsername("ias");
        connectionFactory.setPassword("ias");
        connectionFactory.setVirtualHost("/");
        connectionFactory.setHost("10.3.1.59");
        connectionFactory.setPort(5672);
        Connection connection = (Connection) connectionFactory.newConnection();
        final Channel channel = ((com.rabbitmq.client.Connection) connection).createChannel();
        
        channel.queueDeclare(LB_SM_QUEUE, false, false, false, null);
        
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                            throws IOException {
            	
            	String serviceName = new String(body, "UTF-8");
            		
            	// TODO Fix this min ip issue
            	String chosenServerIP = getMinKey();
            	if(chosenServerIP==null)
            		System.out.println("All Servers Crashed...");
				
            	else
            	{
	            	//String uniqueID = UUID.randomUUID().toString();

			Random rand = new Random();
        		int temp_docker = rand.nextInt(100000);
        		String uniqueID= temp_docker +"";
        		System.out.println(uniqueID);
	            	
	            	JSONObject obj = new JSONObject();
	            	obj.put("service_name", serviceName);
	            	obj.put("docker_id", uniqueID);
	            	
	            	serviceforDocker.put(uniqueID,serviceName);
	            	
	            	System.out.println("Get new request for " + serviceName + " using docker id" + uniqueID);
	            	System.out.println("Forwarding to ip " + chosenServerIP);
	            	
					channel.queueDeclare(chosenServerIP, false, false, false, null);
	                channel.basicPublish("", chosenServerIP,
	                             MessageProperties.PERSISTENT_TEXT_PLAIN,
	                             obj.toString().getBytes("UTF-8"));
	                
	                //Put docker-id of newly spawnned docker to map
	                Vector<String> v = new Vector<String>();
	                if(dockerForIP.get(chosenServerIP) != null)
	                {
	                	v = dockerForIP.get(chosenServerIP);
	                }
	                v.add(uniqueID);
	                
	                dockerForIP.put(chosenServerIP,v);
            	}   
            }
          };
          channel.basicConsume(LB_SM_QUEUE, true, consumer);
	}
	
	// Send spawn request to some(least loaded) running server
	private static void sendSpawnReq(String did) throws IOException {
		
		String chosenServerIP = getMinKey();
		if(chosenServerIP==null)
		{
			System.out.println("All Servers Crashed....");
		}
		else
		{
	    	//String uniqueID = UUID.randomUUID().toString();
	
		Random rand = new Random();
		int temp_docker = rand.nextInt(100000);
		String uniqueID= temp_docker +"";
		System.out.println(uniqueID);
	            	    	
	    	JSONObject obj = new JSONObject();
	    	obj.put("service_name", serviceforDocker.get(did));
	    	obj.put("docker_id", did);
	    	
	    	//System.out.println("Get new request for " + serviceName + " using docker id" + uniqueID);
	    	System.out.println("Forwarding to ip " + chosenServerIP);
	    
			channel.queueDeclare(chosenServerIP, false, false, false, null);
	        channel.basicPublish("", chosenServerIP,
	                     MessageProperties.PERSISTENT_TEXT_PLAIN,
	                     obj.toString().getBytes("UTF-8"));
			
		}
	}
	
	// Checks if some server has crashed
	private static void checkRunningServers() throws InterruptedException, IOException {
		System.out.println("Running servers = "+runningServers);
		for (Entry<String, Boolean> entry : runningServers.entrySet())
		{
		    if(entry.getValue() == false)
		    {
		    	serverInfo.remove(entry.getKey());
		    	runningServers.remove(entry.getKey());
		    	System.out.println("Docker ids corresponding to crashed server = "+dockerForIP.get(entry.getKey()));
		    	
		    	//Re route docker ids to different server
		    	Vector<String> dockers = dockerForIP.get(entry.getKey());
			if (dockers != null) {
		    		for (String did : dockers) {
		    			sendSpawnReq(did);
		    		}
			}

		    	
		    }
		    else {
		    	runningServers.put(entry.getKey(), false);
			}
		}
		Thread.sleep(11000);
	}
	

	
	private static void getPing() throws IOException
	{
//		JSONObject update_info = new JSONObject();
		Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                            throws IOException {
            	
            	String message = new String(body, "UTF-8");
            	
            	JSONParser parser = new JSONParser();
            	JSONObject jo = null;
            	try {
					jo = (JSONObject) parser.parse(message);
				} catch (org.json.simple.parser.ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				};
				

				String ip = (String) jo.get("ip");
				Double cpu = Double.parseDouble((String) jo.get("cpu"));
				serverInfo.put(ip,cpu);
				
				System.out.println("Got ping for " + ip + " with " + serverInfo.get(ip));
				runningServers.put(ip,true);
					
//				updateRoutingInfo();
//				for (Entry<String, Double> entry : serverInfo.entrySet())
//				{
//				    update_info.put(entry.getKey(), entry.getValue());
//				}
//				
//				channel.basicPublish("", LB_SM_QUEUE, null, update_info.toJSONString().getBytes());
				
            }

			
          };
          channel.basicConsume(SERVER_PING_QUEUE, true, consumer);
		
	}

	public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
		// TODO Auto-generated method stub
		
			ServerManager serv_man = new ServerManager();
			serv_man.init();
			
			serv_man.getPing();
			
			serv_man.spawnServiceHandler();
			while(true)
			{
				try {
					checkRunningServers();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
	}
}
