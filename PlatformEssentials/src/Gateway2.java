import java.io.IOException;
import java.util.*;
import java.util.HashMap.*;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import java.lang.reflect.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.net.*;

class NewProducer implements Runnable{
	
	public void run()
	{
		try
		{
			Iterator<String> it = Gateway2.sensors.iterator();
		    while(it.hasNext())
			{
					//System.out.println(entry.getKey());
					Thread object = new Thread(new NewSensors(it.next()));
		
					object.start();
					//	System.out.println("Sensors initialized");
					//		object.close();
			}
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

class NewConsume implements Runnable{
	
	public void run()
	{
		try
		{
		    Iterator<String> it = Gateway2.sensors.iterator();
		    while(it.hasNext())
				{
			//System.out.println(entry.getKey());
			Thread object = new Thread(new NewFetch(it.next()));

			object.start();

		}
		//System.out.println("Sensors initialized");
//		object.close();
	}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	}

class NewFetch implements Runnable{
	private String qname;
	private static ConnectionFactory factory ;

	private static Connection connection ;
	private static Channel channel ;

	public NewFetch(String qn) throws Exception {
		qname = qn;
		//System.out.println(qname);
		factory = new ConnectionFactory();

		factory.setUsername("ias");
         	factory.setPassword("ias");
         	factory.setVirtualHost("/");
		factory.setHost("10.3.1.135");

		factory.setPort(5672);
		connection = factory.newConnection();

		channel = connection.createChannel();

	}
	public void run()
	{
		try
		{
		channel.queueDeclare(qname, false, false, false, null);
	  //  System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

	    Consumer consumer = new DefaultConsumer(channel) {
	      @Override
	      public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
	          throws IOException {
	        String message = new String(body, "UTF-8");
	        Vector<String>rules;
		    rules = Gateway2.sensor_rule.get(qname);

		    try
		    {
		    	Class ruleClass = Class.forName(Gateway2.ruleclassName);
				Object r = ruleClass.newInstance();
	
			    for(String element : rules)
			    {
			       	try
			    	{

			    	 Method set = r.getClass().getMethod(element, String.class);
			         set.invoke(r, message);
			    	}
			    	catch(Exception e)
			    	{
			    		System.out.println(e);
			    	}
			    }
			}
			catch(Exception e)
	    	{
	    		System.out.println("-----------------------"+e);
	    	}
	      }
	    };
	    channel.basicConsume(qname, true, consumer);

	}
	catch(Exception e)
	{
		System.out.println(e);
	}
	}
	
	void close() {

		try {
			channel.close();
			connection.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
	
class NewSensors implements Runnable{
	private String qname;
	private static ConnectionFactory factory ;

	private static Connection connection ;
	private static Channel channel ;

	public NewSensors(String qn) throws Exception {
		qname = qn;

		factory = new ConnectionFactory();


		factory.setUsername("ias");
         	factory.setPassword("ias");
         	factory.setVirtualHost("/");
		factory.setHost("10.3.1.135"); // 10.2.130.64  10.1.100.10

		factory.setPort(5672);

		connection = factory.newConnection();

		channel = connection.createChannel();

	}

	@Override
	public void run()  {
		//System.out.println("Runn");
		try {
			channel.queueDeclare(qname, false, false, false, null);
			int i=10;
			while(i>0)
			{
//				System.out.println(i);
				Random rand = new Random();

				// Generate random integers in range 0 to 999
				int rand_int = rand.nextInt(1000);

				channel.basicPublish("", qname, null, Integer.toString(rand_int).getBytes());
				i--;

			}
//			System.out.println(" [x] Sent " );

		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	void close()
	{

		try {
			channel.close();
			connection.close();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

public class Gateway2
{
	static String myip, myname;
	public static Vector<String> sensors;
	public static Map< String, Vector<String> > sensor_rule;
	public static String ruleclassName = "";	
	
	public static void sensor_rule_map(String filename)
	{
		sensor_rule = new HashMap< String, Vector<String> >();
		String filePath = filename;
		File xmlFile = new File(filePath);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;

		try
		{
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);
			doc.getDocumentElement().normalize();

			NodeList classList = doc.getElementsByTagName("class-name");
			ruleclassName = classList.item(0).getTextContent().toString();

			NodeList sensorList = doc.getElementsByTagName("sensor-id");
			
			for (int i=0;i<sensorList.getLength();i++)
			{
				NodeList idValue = ((Element)sensorList.item(i)).getElementsByTagName("id");
				String id = idValue.item(0).getTextContent().toString();

				NodeList rules = ((Element)sensorList.item(i)).getElementsByTagName("rule");

				Vector<String> rulelist = new Vector<String>();
				for(int j=0; j<rules.getLength(); j++)
				{
					Node rule = rules.item(j);
					String str = rule.getTextContent().toString();
					rulelist.add(str);
				}

				sensor_rule.put(id, rulelist);
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}
	
	public static void findIP() throws SocketException
	{
		boolean flag = false;
		for (final Enumeration< NetworkInterface > interfaces =NetworkInterface.getNetworkInterfaces( );interfaces.hasMoreElements( );)
		{
	   		final NetworkInterface cur = interfaces.nextElement( );
	   		if ( cur.isLoopback( ) )
	   			continue;

		    for ( final InterfaceAddress addr : cur.getInterfaceAddresses( ) )
	    	{
		        final InetAddress inet_addr = addr.getAddress( );
		        if ( !( inet_addr instanceof Inet4Address ) )
	        		continue;
		        String ip =inet_addr.getHostAddress();
//		        System.out.println(ip);
	        	if (ip.startsWith("10."))
			    {
	//		       System.out.println(myip);
//			       	flag = true;
	        		myip = ip;
		        	
	    		}
	    	}
	  //  	if(flag)
	  //  		break;
		}
	}
	
	static void sensor_map(String f1,String f2) throws SocketException
	{
		findIP();
		System.out.println("My ip is = " + myip);
		sensors = new Vector<String>();
		File systemFile = new File(f1);
		File gatewayFile = new File(f2);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;

		try
		{

			// Finding gateway name
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(systemFile);
			doc.getDocumentElement().normalize();

			NodeList gateways = doc.getElementsByTagName("Gateways");
			// System.out.println("Number of gateways : " +gateways.getLength());

			NodeList gatewayList = ((Element)gateways.item(0)).getElementsByTagName("Gateway");
			System.out.println("Number of gateways : "+gatewayList.getLength());
			
			for (int i=0;i<gatewayList.getLength();i++)
			{
				NodeList ipaddress = ((Element)gatewayList.item(i)).getElementsByTagName("IP");
				String ip = ipaddress.item(0).getTextContent().toString();

				if(ip.equals(myip))
				{
					NodeList name = ((Element)gatewayList.item(i)).getElementsByTagName("Name");
					myname = name.item(0).getTextContent().toString();
					System.out.println("IP : "+ ip+"\n Name : "+myname);
					break;
				}
			}

			// finding the sensors associated with this gateway
			doc = dBuilder.parse(gatewayFile);
			doc.getDocumentElement().normalize();

			gatewayList = doc.getElementsByTagName("Mapping");
			System.out.println("Number of Nodes in XML File: "+gatewayList.getLength());
			
			for (int i=0;i<gatewayList.getLength();i++)
			{
				NodeList gateway = ((Element)gatewayList.item(i)).getElementsByTagName("Gateway");
				String name = gateway.item(0).getTextContent().toString();

				if(name.equals(myname))
				{
					System.out.println(myname);
					NodeList sensor = ((Element)gatewayList.item(i)).getElementsByTagName("Sensor");
					for(int j=0; j<sensor.getLength(); j++)
					{
						Node ss = sensor.item(j);
						sensors.add(ss.getTextContent().toString());
					}
					break;
				}
			}

			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws Exception
	{
		sensor_map("new-system-info.xml","new-gateway-sensor.xml");
		sensor_rule_map("new-sensor-rule-map.xml");
		// Ask 
		Rule r = new Rule();
		System.out.println("Calling event init");
		System.out.println(sensors.size());
		r.init();

		Thread prod = new Thread(new NewProducer());
		prod.start();
		Thread cons = new Thread(new NewConsume());
		cons.start();
	   
	}
}


