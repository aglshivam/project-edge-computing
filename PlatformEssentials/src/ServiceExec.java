import java.lang.reflect.*;
public class ServiceExec {
	public static void main(String args[]){
		String methodName = args[0];
		Service srv = new Service();
		try {
			System.out.println("Service invoking "+methodName);
			Method c = srv.getClass().getMethod(methodName, String.class);
			c.invoke(srv);
		}catch(Exception e)
		{
			System.out.println("Exception");
		} 
	}
}