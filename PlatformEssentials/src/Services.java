import java.io.File;
import java.io.IOException; 
import java.util.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Services
{
	static Map< String, Vector<String> > services;

	Services(String filename)
	{
		services = new HashMap< String, Vector<String> >();
		String filePath = filename;
		File xmlFile = new File(filePath);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;

		try
		{
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);
			doc.getDocumentElement().normalize();

			NodeList serviceMetaData = doc.getElementsByTagName("Service");
			
			for (int i=0;i<serviceMetaData.getLength();i++)
			{
				NodeList serviceName = ((Element)serviceMetaData.item(i)).getElementsByTagName("Name");
				String name = serviceName.item(0).getTextContent().toString();

				Vector<String> metadata = new Vector<String>();
				String str;

				NodeList priority = ((Element)serviceMetaData.item(i)).getElementsByTagName("Priority");
				str = priority.item(0).getTextContent().toString();
				metadata.add(str);

				NodeList returnType = ((Element)serviceMetaData.item(i)).getElementsByTagName("Return-type");
				str = returnType.item(0).getTextContent().toString();
				metadata.add(str);

				NodeList parameterType = ((Element)serviceMetaData.item(i)).getElementsByTagName("Parameter-Type");
				
				for(int j=0; j<parameterType.getLength(); j++)
				{
					str = parameterType.item(j).getTextContent().toString();
					metadata.add(str);
				}
				services.put(name, metadata);
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
       
	public static void main(String[] args)
	{
		Services er = new Services("services.xml");
	}
}