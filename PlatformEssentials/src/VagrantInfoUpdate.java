import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.concurrent.TimeoutException;

import org.json.simple.JSONObject;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class VagrantInfoUpdate {
	private final static String SERVER_PING_QUEUE = "server_incoming"; // Service manager queue

	private static ConnectionFactory factory;
	private static Connection connection;
	private static Channel channel;
	static String findUsage() {
		String str = "";
		  OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
		  for (Method method : operatingSystemMXBean.getClass().getDeclaredMethods()) {
		    method.setAccessible(true);
		    if (method.getName().startsWith("get")
		        && Modifier.isPublic(method.getModifiers())) {
		            Object value;
		        try {
		            value = method.invoke(operatingSystemMXBean);
		        } catch (Exception e) {
		            value = e;
		        } 
		        str=method.getName() + " = " + value;
		        if(str.contains("getSystemCpuLoad"))	
		          	break;
		          
		        //System.out.println(method.getName() + " = " + value);
		    } 
		  } 
		  return str;
		}
	
	static void sendPing(String cpu) throws IOException
	{
		JSONObject ping = new JSONObject();
		ping.put("ip", "10.3.0.58");
		ping .put("cpu",cpu );
		
		System.out.println("Vagrind info ping " +  ping);
		
		channel.basicPublish("", SERVER_PING_QUEUE, null, ping.toJSONString().getBytes());
	}
	static void init() throws IOException, TimeoutException
	{
		factory = new ConnectionFactory();

		connection = factory.newConnection();
		channel = connection.createChannel();
        factory.setUsername("ias");
        factory.setPassword("ias");
        factory.setVirtualHost("/");
        factory.setPort(5672);
		factory.setHost("10.3.0.58");
		channel.queueDeclare(SERVER_PING_QUEUE, false, false, false, null);
		
	}
}
