import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.TimeoutException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;


public class ServiceManager {
	
	public final static String SERVICE_PING_QUEUE = "sm_incomming"; // Service manager queue
	public static final String LB_SM_QUEUE = "lb_sm_conn"; // Load balancer - Service manager queue
	public static ConnectionFactory connectionFactory;
	public static Connection send_connection;
	public static Channel send_channel;
	
	// We will maintain a count of number of docker instances. We need this to keep track of how many docker instances have updated their status via ping 
	//public int numberOfDockerInstances = 2;
	public int numberOfStatusUpdates;
	
	Object syncObj = new Object();
	
	// This data structure will hold the routing information
	// The first hash map will have service names as keys and it value will be another hash map for docker ids hosting the service.
	// The second hash map will have docker id as its key and the number of pending requests in its queue as its value.
	public HashMap<String, HashMap<String, Integer > > routingTable = new HashMap<String, HashMap<String, Integer > >();
	
	public static double scale(double x)
	{
	    return x * 100;
	}
	
	public void init() throws IOException, TimeoutException {
		// Setting up the send channel for updating to load balancer
		connectionFactory = new ConnectionFactory();
		connectionFactory.setUsername("ias");
		connectionFactory.setPassword("ias");
        connectionFactory.setVirtualHost("/");
        

        connectionFactory.setHost("10.3.1.59");

        connectionFactory.setPort(5672);
        send_connection = connectionFactory.newConnection();
        send_channel = send_connection.createChannel();
        send_channel.queueDeclare(LB_SM_QUEUE, false, false, false, null);
        
	}
	
		// Method will continually read the queue and route the requests based on the routing table

	public synchronized void getPing() {
		// TODO Auto-generated method stub
		InetAddress inetAddress = null;
		try {
			inetAddress = InetAddress.getLocalHost();
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setUsername("ias");
        connectionFactory.setPassword("ias");
        connectionFactory.setVirtualHost("/");
        connectionFactory.setHost("10.3.1.59");
        connectionFactory.setPort(5672);
        Connection connection = null;
		try {
			connection = (Connection) connectionFactory.newConnection();
		} catch (IOException | TimeoutException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
        Channel channel = null;
		try {
			channel = ((com.rabbitmq.client.Connection) connection).createChannel();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

        try {
			channel.queueDeclare(SERVICE_PING_QUEUE, false, false, false, null);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                            throws IOException {
            	
            	String message = new String(body, "UTF-8");
            	
            	JSONParser parser = new JSONParser();
            	JSONObject jo = null;
            	try {
					jo = (JSONObject) parser.parse(message);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				};
				
				String serviceName = (String) jo.get("service_name");
				String dockerId = (String) jo.get("docker_id");
				String weight = (String) jo.get("weight");
				
				// TODO: Update the service registry/ routing information
				double val = Double.parseDouble(weight);
				double scaledUpWeight = scale(val);
				if (!routingTable.containsKey(serviceName)) {
					routingTable.put(serviceName, new HashMap<String, Integer>());
				}
				routingTable.get(serviceName).put(dockerId, new Integer((int)scaledUpWeight));
				
				numberOfStatusUpdates++;
				
				System.out.println("numberOfStatusUpdates = " + numberOfStatusUpdates);
				
				// TODO: Handle base case when numberOfDockerInstances is 0
				System.out.println(ServerManager.noOfAgents);
				if (true/*numberOfStatusUpdates == ServerManager.noOfAgents*/) {
					
					// Notify updateRoutingInformation
					numberOfStatusUpdates = 0;
					synchronized(syncObj) {
						syncObj.notify();
					}
				}
            }
          };
          try {
			channel.basicConsume(SERVICE_PING_QUEUE, true, consumer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public  void updateRoutingInformation() {
	    try {
	        // wait() will block this thread until (numberOfStatusUpdates == numberOfDockerInstances) is true
	    	synchronized(syncObj) {
	    		syncObj.wait();
	    	}
	    	
	    	System.out.println("notified");
	        
	        Iterator it = routingTable.entrySet().iterator();
	        
	        System.out.println(routingTable.size());
	        
	        String serviceName = null;
	        
	        JSONObject obj = new JSONObject();
	        JSONArray temp_ja1 = new JSONArray();
	        
	        while (it.hasNext()) {
	        	Map.Entry pair = (Map.Entry)it.next();
	        	System.out.println(pair);
	        	serviceName = (String) pair.getKey();
	        	Object[] a = ((Map) pair.getValue()).entrySet().toArray();
	        	
	        	// Sort the docker instances based on weights for this service
	        	Arrays.sort(a, new Comparator<Object>() {
		            public int compare(Object o1, Object o2) {
		                Entry<String, Integer> entry = (Map.Entry<String, Integer>) o2;
						return entry.getValue()
		                           .compareTo(((Map.Entry<String, Integer>) o1).getValue());
		            }
		        });
	        	
	        	JSONArray temp_ja = new JSONArray();
	        	JSONObject temp_jo1 = new JSONObject();
	        	JSONObject temp_jo2 = null;
	        	String dockerId = null;
	        	int weight;
	        	
	        	temp_jo1.put("service_name", serviceName);
		        for (Object e : a) {
		        	dockerId = ((Map.Entry<String, Integer>) e).getKey();
		        	weight = ((Map.Entry<String, Integer>) e).getValue();
		        	
		        	for (int i = 0; i < weight; i++) {
		        		temp_jo2 = new JSONObject();
		        		temp_jo2.put("queue_name", dockerId);
		        		temp_ja.add(temp_jo2);
		        	}
		        }
		        temp_jo1.put("docker_instances", temp_ja);
		        temp_ja1.add(temp_jo1);
	        }
	        obj.put("services_active", temp_ja1);
	        
	        
	        System.out.println(obj.toString());
	        		        
	        send_channel.basicPublish("", LB_SM_QUEUE, null, obj.toString().getBytes("UTF-8"));
	        
	    } catch (InterruptedException e) {
	        // Happens if someone interrupts your thread.
	    	System.out.println(e);
	    } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
