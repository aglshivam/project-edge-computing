
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ayushi
 */

import java.io.*;
import java.util.*;

public class Rule1 extends Rule{

    /**
     * @param args the command line arguments
     */
    int data;
    boolean res;
  
    Rule r=new Rule();
    public void rule1(String d) throws IOException{
        int x=Integer.parseInt(d);
       
        if(x>100)
            r.putRuleData("rule1","true");
        else
            r.putRuleData("rule1","false");
    }
    public void rule2(String d) throws IOException{
        int x = Integer.parseInt(d);
        if(x<=50&&x>40)
            r.putRuleData("rule2","true");
        else
            r.putRuleData("rule2","false");
            
    }
    public void rule3(String d) throws IOException{
        int x = Integer.parseInt(d);
        if(x<=40&&x>30)
            r.putRuleData("rule3","true");
        else
            r.putRuleData("rule3","false");
            
    }
    public void rule4(String d) throws IOException{
        int x = Integer.parseInt(d);
        if(x<=30&&x>20)
            r.putRuleData("rule4","true");
        else
            r.putRuleData("rule4","false");
            
    }
    public void rule5(String d) throws IOException{
        int x =Integer.parseInt(d);
        if(x<=20&&x>10)
            r.putRuleData("rule5","true");
        else
            r.putRuleData("rule5","false");
            
    }
    public void rule9(String d) throws IOException{
        int x = Integer.parseInt(d);
        if(x<=10&&x>0)
            r.putRuleData("rule9","true");
        else
            r.putRuleData("rule9","false");
            
    }
    

    public static void main(String[] args)throws Exception {
        // TODO code application logic here
      
    }
    private static void call_service1(int x){
        System.out.println("x is greater than 100");
    }
    private static void call_service2(int x){
       System.out.println("x is <=50 and x>=0");
    }
    
    
}
