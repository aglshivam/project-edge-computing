import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.TimeoutException;
import com.rabbitmq.client.GetResponse;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class EventManager {
	
	static Map< String, Vector<String> > event_rule;
	private static ConnectionFactory factory ;
	public static Map< String, Vector<String> > services_map;
	private static Connection connection ;
	private static Channel channel ;
	public static String eventclassName = "";
	String msg = "";
	public static Vector<String> rules;
	PlatformAgent p ;
	protected  void process_event() {}


	public static void event_rule_map(String filename)
	{
		event_rule = new HashMap< String, Vector<String> >();
		String filePath = filename;
		File xmlFile = new File(filePath);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;

		try
		{
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);
			doc.getDocumentElement().normalize();

			NodeList classList = doc.getElementsByTagName("class-name");
			eventclassName = ((Element)classList.item(0)).getTextContent().toString();

			NodeList eventList = doc.getElementsByTagName("Event");
			
			for (int i=0;i<eventList.getLength();i++)
			{
				NodeList eventName = ((Element)eventList.item(i)).getElementsByTagName("event-name");
				String event = eventName.item(0).getTextContent().toString();

				NodeList rulesRequired = ((Element)eventList.item(i)).getElementsByTagName("rules-required");
				NodeList rules = ((Element)rulesRequired.item(0)).getElementsByTagName("rule");


				Vector<String> rulelist = new Vector<String>();
				for(int j=0; j<rules.getLength(); j++)
				{
					Node rule = rules.item(j);
					String str = rule.getTextContent().toString();
					rulelist.add(str);
				}

				event_rule.put(event, rulelist);
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}
	private static void read_rules(String filename)
	{
		rules = new Vector<String>();
		String filePath = filename;
		File xmlFile = new File(filePath);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;

		try
		{
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);
			doc.getDocumentElement().normalize();

			NodeList ruleList = doc.getElementsByTagName("rule-name");
			
			for (int i=0;i<ruleList.getLength();i++)
			{
				String str = ((Node)ruleList.item(i)).getTextContent().toString();
				rules.add(str);
			}
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
	}

	public static void main(String []args) throws IOException, TimeoutException
	{
		event_rule_map("event-rule-map.xml");
		read_rules("rules.xml");
		factory = new ConnectionFactory();
		factory.setHost("localhost");
		connection = factory.newConnection();
		channel = connection.createChannel();
		System.out.println("Creating PlatformAgent Object");
//		p = new PlatformAgent();
		
		for(String rule:rules)
		{
			channel.exchangeDeclare(rule,"fanout");
		}
		
		for (Map.Entry<String, Vector<String>> entry : event_rule.entrySet())
		{
			Vector<String> rul = entry.getValue();
			for(String rule:rul)
			{
				channel.queueDeclare(entry.getKey()+rule, false, false, false, null);
				channel.queueBind(entry.getKey()+rule, rule, "");
					
			}
		}
//		readServiceMap("services.xml");
//		for (Map.Entry<String, Vector<String>> entry : services_map.entrySet())
//		{
//			Vector<String>val = entry.getValue();
//			System.out.println(val.elementAt(val.size()-1));
//			channel.queueDeclare(val.elementAt(val.size()-1), false, false, false, null);
//			
//		}
	
		
          
	}
}

