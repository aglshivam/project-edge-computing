import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeoutException;

import org.json.simple.JSONObject;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;



public class DockerInfoUpdateAgent {
	
	// TODO: Create a unified root password for all docker instances
	String password = "shubhi";
	
	// TODO: The queue name should be the docker Id
	String serviceQueueName;
	
	String serviceName ;
	
	private final static String SERVICE_PING_QUEUE = "sm_incomming"; // Service manager queue
	private static ConnectionFactory connectionFactory;
	private static Connection send_connection;
	private static Channel send_channel;
	
	Integer numberOfRequests = new Integer(0);
	Integer noOfUnacknowledgedRequests = new Integer(0);
	
	public DockerInfoUpdateAgent(String id, String serviceN) {
		// TODO Auto-generated constructor stub
		serviceQueueName = id;
		serviceName = serviceN;
	}
	
	
	void init() throws IOException, TimeoutException {
		// Initialize the message queue for service manager
		connectionFactory = new ConnectionFactory();
		connectionFactory.setUsername("ias");
		connectionFactory.setPassword("ias");
        connectionFactory.setVirtualHost("/");
        
        // TODO: Get the IP of the service manager from the deployer script
        connectionFactory.setHost("10.3.0.58");
        connectionFactory.setPort(5672);
        send_connection = connectionFactory.newConnection();
        send_channel = send_connection.createChannel();
        send_channel.queueDeclare(SERVICE_PING_QUEUE, false, false, false, null);
	}
	
	
	
	// Method will ping the service manager about how many requests are there in its queue
	void pingServiceManger() throws IOException, TimeoutException {
		
        JSONObject obj = new JSONObject();
		obj.put("docker_id", serviceQueueName);
		Double weight = null;
		if (noOfUnacknowledgedRequests == 0 || numberOfRequests == 0) {
			weight = new Double(0.5);
		} else if (noOfUnacknowledgedRequests == numberOfRequests && noOfUnacknowledgedRequests != 0) {
			weight = new Double(0);
		} else {
			weight = new Double((double)noOfUnacknowledgedRequests / numberOfRequests);
		}
		obj.put("weight", weight.toString());
		obj.put("service_name", serviceName);
		
		System.out.println("Pinging service manager with msg " + obj);
        
        send_channel.basicPublish("", SERVICE_PING_QUEUE, null, obj.toString().getBytes());
	}
	
	void findNumberOfPendingRequests() throws IOException {
		String[] cmd = {
				"/bin/sh",
				"-c",
				"echo \"" + password + "\" | sudo -S rabbitmqctl list_queues | grep " + serviceQueueName + " | awk \'{print $2}\'"
		};
		Process p1 = Runtime.getRuntime().exec(cmd);
		BufferedReader buffReader1 = new BufferedReader(new InputStreamReader(p1.getInputStream()));
		String noreq = buffReader1.readLine();
		System.out.println("no of requests ===== "+noreq);
		int no_of_requests = Integer.parseInt(noreq);
		numberOfRequests = new Integer(no_of_requests);
		String[] cmd2 = {
				"/bin/sh",
				"-c",
				"echo \"" + password + "\" | sudo -S rabbitmqctl list_queues name messages_unacknowledged | grep " + serviceQueueName + " | awk \'{print $2}\'"
		};
		Process p2 = Runtime.getRuntime().exec(cmd);
		BufferedReader buffReader2 = new BufferedReader(new InputStreamReader(p2.getInputStream()));
		int no_of_unacknowledged_requests = Integer.parseInt(buffReader2.readLine());
		noOfUnacknowledgedRequests = new Integer(no_of_unacknowledged_requests);
	}
}
