import subprocess as sb
import sys

RFILE_NAME = "Rule.java"
#GFILE_NAME = "Gateway1.java"
APP_RFILE_NAME = "Rule1.java"
NEW_SYSTEM_INFO = "new-system-info.xml"
NEW_GATEWAY_SENSOR = "new-gateway-sensor.xml"
NEW_SENSOR_RULE = "new-sensor-rule-map.xml"
AMQ_CLIENT = "amqp-client-4.0.2.jar"
RABBIT_CLIENT = "rabbitmq-client.jar"
SLF4J_21 = "slf4j-api-1.7.21.jar"
SLF4J_22 = "slf4j-simple-1.7.22.jar"
JSON = "json-simple-1.1.1.jar"
GATEWAY2 = "Gateway2.java"
RUNNER = "gateway_runner.sh"


sName = sys.argv[1]
sPass = sys.argv[2]
ip = sys.argv[3]

# TO Path
TO_CPATH = 'PlatformC/'
EXPORTS = ".:amqp-client-4.0.2.jar:slf4j-api-1.7.21.jar:slf4j-simple-1.7.22.jar:json-simple-1.1.1.jar"

cmd = 'sshpass -p \"'+sPass+'\" ssh ' + sName + '@' + ip + ' mkdir '+TO_CPATH
sb.call(cmd,shell=True)

# SCP files
cmd = 'sshpass -p \"' + sPass + '\" scp ' + RFILE_NAME + ' ' + sName + '@' + ip + ':~/' + TO_CPATH + RFILE_NAME
sb.call(cmd,shell=True)
# cmd = 'sshpass -p \"' + sPass + '\" scp ' + GFILE_NAME + ' ' + sName + '@' + ip + ':' + GFILE_NAME
# sb.call(cmd,shell=True)
cmd = 'sshpass -p \"' + sPass + '\" scp ' + APP_RFILE_NAME + ' ' + sName + '@' + ip + ':~/' + TO_CPATH + APP_RFILE_NAME
sb.call(cmd,shell=True)
cmd = 'sshpass -p \"' + sPass + '\" scp ' + NEW_SYSTEM_INFO + ' ' + sName + '@' + ip + ':~/' + TO_CPATH + NEW_SYSTEM_INFO
sb.call(cmd,shell=True)
cmd = 'sshpass -p \"' + sPass + '\" scp ' + NEW_GATEWAY_SENSOR + ' ' + sName + '@' + ip + ':~/' + TO_CPATH + NEW_GATEWAY_SENSOR
sb.call(cmd,shell=True)
cmd = 'sshpass -p \"' + sPass + '\" scp ' + NEW_SENSOR_RULE + ' ' + sName + '@' + ip + ':~/' + TO_CPATH + NEW_SENSOR_RULE
sb.call(cmd,shell=True)
cmd = 'sshpass -p \"' + sPass + '\" scp ' + AMQ_CLIENT + ' ' + sName + '@' + ip + ':~/' + TO_CPATH + AMQ_CLIENT
sb.call(cmd,shell=True)
cmd = 'sshpass -p \"' + sPass + '\" scp ' + RABBIT_CLIENT + ' ' + sName + '@' + ip + ':~/' + TO_CPATH + RABBIT_CLIENT
sb.call(cmd,shell=True)
cmd = 'sshpass -p \"' + sPass + '\" scp ' + SLF4J_21 + ' ' + sName + '@' + ip + ':~/' + TO_CPATH + SLF4J_21
sb.call(cmd,shell=True)
cmd = 'sshpass -p \"' + sPass + '\" scp ' + SLF4J_22 + ' ' + sName + '@' + ip + ':~/' + TO_CPATH + SLF4J_22
sb.call(cmd,shell=True)
cmd = 'sshpass -p \"' + sPass + '\" scp ' + JSON + ' ' + sName + '@' + ip + ':~/' + TO_CPATH + JSON
sb.call(cmd,shell=True)
cmd = 'sshpass -p \"' + sPass + '\" scp ' + GATEWAY2 + ' ' + sName + '@' + ip + ':~/' + TO_CPATH + GATEWAY2
sb.call(cmd,shell=True)
cmd = 'sshpass -p \"' + sPass + '\" scp ' + RUNNER + ' ' + sName + '@' + ip + ':~/' + TO_CPATH + RUNNER
sb.call(cmd,shell=True)

cmd = 'sshpass -p \"'+sPass+'\" ssh ' + sName + '@' + ip + ' bash /home/'+sName+'/'+TO_CPATH+'/'+RUNNER+' '+sName
sb.call(cmd,shell=True)

# # Compile and run 
# cmd = 'sshpass -p \"'+sPass+'\" ssh ' + sName + '@' + ip + ' cd /home/'+sName+'/'+TO_CPATH+' ; javac -cp '+ EXPORTS + ' ' + RFILE_NAME
# sb.call(cmd,shell=True)
# print cmd
# cmd = 'sshpass -p \"'+sPass+'\" ssh ' + sName + '@' + ip + ' cd /home/'+sName+'/'+TO_CPATH+' ; javac -cp '+ EXPORTS + ' ' + APP_RFILE_NAME
# sb.call(cmd,shell=True)
# print cmd
# cmd = 'sshpass -p \"'+sPass+'\" ssh ' + sName + '@' + ip + ' cd /home/'+sName+'/'+TO_CPATH+' ; javac -cp '+ EXPORTS + ' ' + GATEWAY2 + ';nohup java -cp '+ EXPORTS + ' ' + GATEWAY2.split('.')[0] + ' &'
# sb.call(cmd,shell=True)
# print cmd
