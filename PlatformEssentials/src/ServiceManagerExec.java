import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.TimeoutException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

class T1 implements Runnable {
   ServiceManager sm;

   public T1(ServiceManager m1) {
      this.sm = m1;
      new Thread(this, "FetchPings").start();
   }

   public void run() {
	   sm.getPing();
   }
}

class T2 implements Runnable {
   ServiceManager sm;

   public T2(ServiceManager m2) {
      this.sm = m2;
      new Thread(this, "UpdateRoute").start();
   }

   public void run() {
	   while(true) {
		   sm.updateRoutingInformation();
	   }
   }
}

class ServiceManagerExec {
	public static void main(String[] args) throws IOException, TimeoutException {
		// TODO: Create a thread for monitoring pings
		
		ServiceManager sm = new ServiceManager();
		sm.init();
		new T1(sm);
		new T2(sm);
	}
}
