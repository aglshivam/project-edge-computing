import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.util.*;
import java.util.concurrent.TimeoutException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.MessageProperties;


public class PlatformAgent {
	
	private static final String LB_QUEUE = "lb_incomming";
	private static ConnectionFactory connectionFactory;
	private static Connection connection;
	private static Channel channel;
	
	void init() throws IOException, TimeoutException {
		connectionFactory = new ConnectionFactory();
		connectionFactory.setUsername("ias");
		connectionFactory.setPassword("ias");
        connectionFactory.setVirtualHost("/");
        
        // TODO: Get the IP of the load balancer from the deployer script
        connectionFactory.setHost("10.3.0.58");
        connectionFactory.setPort(5672);
        connection = connectionFactory.newConnection();
        channel = connection.createChannel();
        channel.queueDeclare(LB_QUEUE, false, false, false, null);
        
	}
	
//	// Method will accept request for spawning new services
//		void spawnServiceHandler() throws IOException, TimeoutException {
//			InetAddress inetAddress = InetAddress.getLocalHost();
//	        ConnectionFactory connectionFactory = new ConnectionFactory();
//	        connectionFactory.setUsername("guest");
//	        connectionFactory.setPassword("guest");
//	        connectionFactory.setVirtualHost("/");
//	        connectionFactory.setHost("localhost");
//	        connectionFactory.setPort(5672);
//	        Connection connection = (Connection) connectionFactory.newConnection();
//	        final Channel channel = ((com.rabbitmq.client.Connection) connection).createChannel();
//	        
//	        
//					
//	            	//String uniqueID = UUID.randomUUID().toString();
//	        		Random rand = new Random();
//	        		int temp_docker = rand.nextInt(100000);
//	        		String uniqueID= temp_docker +"";
//	        		System.out.println(uniqueID);
//	            	
//	            	JSONObject obj = new JSONObject();
//	            	obj.put("service_name", "foo");
//	            	obj.put("docker_id", uniqueID);
//	            	
//	            	System.out.println("Get new request for foo using docker id" + uniqueID);
//	            	System.out.println("Forwarding to ip localhost");
//	            	
//					channel.queueDeclare("localhost", false, false, false, null);
//	                channel.basicPublish("", "localhost",
//	                             MessageProperties.PERSISTENT_TEXT_PLAIN,
//	                             obj.toString().getBytes("UTF-8"));
//		}
	
	// Method send the request to call the service to the load balancer
	void callService(JSONObject obj) throws IOException, TimeoutException {
		// TODO: If service is present in local host do not request load balancer, inform service manager and invoke service from here only.
        
		System.out.println("obj = " + obj);
		
        channel.basicPublish("", LB_QUEUE, null, obj.toString().getBytes("UTF-8"));
        
        
	}
	
	// Method send the request to call the service to the load balancer
		void callWorkflow(String obj) throws IOException, TimeoutException {
			// TODO: If workflow is present in local host do not request load balancer, inform service manager and invoke workflow from here only.
			
			connectionFactory = new ConnectionFactory();
			connectionFactory.setUsername("ias");
			connectionFactory.setPassword("ias");
	        connectionFactory.setVirtualHost("/");
	        
	        // TODO: Get the IP of the load balancer from the deployer script
	        connectionFactory.setHost("10.3.0.58");
	        connectionFactory.setPort(5672);
	        connection = connectionFactory.newConnection();
	        channel = connection.createChannel();
	        channel.queueDeclare(LB_QUEUE, false, false, false, null);
	        
	        
	        channel.basicPublish("", LB_QUEUE, null, obj.toString().getBytes("UTF-8"));
	        
	        
	}
	
		
	//spawn service reads from queue named server ip. when a new msg is received 
		
	public void readQueue() throws IOException, TimeoutException {
		connectionFactory = new ConnectionFactory();
		connectionFactory.setUsername("ias");
		connectionFactory.setPassword("ias");
        connectionFactory.setVirtualHost("/");
        
        // TODO: Get the IP of this machine on which this agent is running from the deployer script
        connectionFactory.setHost("10.3.0.58");
        connectionFactory.setPort(5672);
        connection = connectionFactory.newConnection();
        channel = connection.createChannel();

        System.out.println("Started reading req queue");
		
        channel.queueDeclare("10.3.0.58", false, false, false, null);
        
		Consumer consumer = new DefaultConsumer(channel) {
			  @Override
			  public void handleDelivery(String consumerTag, Envelope envelope,
			                             AMQP.BasicProperties properties, byte[] body)
			      throws IOException {
			    String message = new String(body, "UTF-8");
			    
			    JSONParser parser = new JSONParser();
            	JSONObject jo = null;
            	try {
					jo = (JSONObject) parser.parse(message);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				};
				
				String docker_id = (String) jo.get("docker_id");
				String serviceName = (String) jo.get("service_name");
				
				System.out.println("Spawning new docker instance with id" + docker_id + " for service " + serviceName);
			
				String pass = "shubhi";
	
				// Script to spawn docker for service
				String command = "echo \""+pass+"\" |sudo docker build -t "+docker_id+" .";
				Process p = Runtime.getRuntime().exec(new String[]{ "bash", "-c", command });
				System.out.print(command);
				
				String line;
				
				BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
				  while ((line = input.readLine()) != null) {
				    System.out.println(line);
				  }
				
				System.out.println("\n\n\n Now running doc agent\n\n\n");
				
				DockerAgent da = new DockerAgent();

				da.docker_id = docker_id;
				da.methodName = serviceName;
				
				try {
					System.out.println("Starting docker agent for this service");
					da.processRequest();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				};
			    
			    System.out.println(" [x] Received '" + message + "'");
			  }
			};
			channel.basicConsume("10.3.0.58", true, consumer);

		
	}
		
	public static void main(String[] args) throws IOException, TimeoutException {
		
		// Any event that wishes to call a service will create a PlatformAgent instance and call the callSerice function on it.
		PlatformAgent pa = new PlatformAgent();
		
		pa.readQueue();
		
//		pa.spawnServiceHandler();
		
		VagrantInfoUpdate viu = new VagrantInfoUpdate();
		
		viu.init();
		while(true)
		{
			String usage = viu.findUsage();
			String[] use = usage.split(" = ");
			System.out.println(use[1]);
			viu.sendPing(use[1]);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
//		JSONObject obj = new JSONObject();
//		obj.put("service_name", "foo");
//		
////		JSONObject obj2 = new JSONObject();
////		obj2.put("service_name", "bar");
//		
//		for(int i = 0; i < 1000; i++) {
//			pa.callService(obj);
//		}
//		pa.callWorkflow(obj2);
		
	}
}
