import java.io.*;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.util.concurrent.TimeoutException;
import java.io.*;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class DockerAgent {
	
	static String docker_id;
	static String methodName;
	static DockerInfoUpdateAgent dia;
	
	static void processRequest() throws IOException, TimeoutException {
		ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        connectionFactory.setVirtualHost("/");
        connectionFactory.setHost("localhost");
        connectionFactory.setPort(5672);
        Connection connection = (Connection) connectionFactory.newConnection();
        Channel channel = ((com.rabbitmq.client.Connection) connection).createChannel();
        
        dia = new DockerInfoUpdateAgent(docker_id, methodName);

        dia.init();
        
        dia.pingServiceManger();
        
        System.out.println("creating service queue name " + docker_id);

        channel.queueDeclare(docker_id, false, false, false, null);
        
        Consumer consumer = new DefaultConsumer(channel) {
        	int i = 0;
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                            throws IOException {
            	
            	String message = new String(body, "UTF-8");
            	
            	try {
					dia.pingServiceManger();
				} catch (TimeoutException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            	
            	JSONParser parser = new JSONParser();
            	JSONObject jo = null;
            	try {
					jo = (JSONObject) parser.parse(message);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				};
				
				String pass = "shubhi";

				String serviceName = (String) jo.get("service_name");

				System.out.println("Got new request for service" + serviceName);
				
				// Process service by calling the service method here
				
				//String command = "echo \"1234\" |sudo docker run --name ab -e var1=\"india\" --net=host shivam1";
				String command = "echo \""+pass+"\" | nohup sudo docker run -e var1="+serviceName+" --net=host "+docker_id+" &";
				Process p = Runtime.getRuntime().exec(new String[]{ "bash", "-c", command });
				System.out.println(command);
				
				
//				ProcessBuilder pb1 = new ProcessBuilder("sudo","nohup","docker","run","--name test1","var1=test","--net=host","test");
//				Process p1 = pb1.start();
//				System.out.print(pb1.command());
				
				try {
					p.waitFor();
        				int returnValue = p.exitValue();
					System.out.println("Docker process exited with return val = " + returnValue);
				} catch (InterruptedException e) {
        				e.printStackTrace();
    				}
				String line1;
				
				BufferedReader input1 = new BufferedReader(new InputStreamReader(p.getInputStream()));
				  while ((line1 = input1.readLine()) != null) {
				    System.out.println(line1);
				  }
				i++;
				
            }
          };
          channel.basicConsume(docker_id, true, consumer);
	}

}
