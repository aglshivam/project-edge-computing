import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.TimeoutException;
import com.rabbitmq.client.GetResponse;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


class Events implements Runnable{
	private String event_name;
	public Events(String e) throws Exception {
      		event_name=e;
	}
	
	public void run()
	{
		try
    	{
			Class eventClass = Class.forName(Event.eventclassName);
			Object ex = eventClass.newInstance();
		
	        System.out.println(event_name);
	    	Method set = ex.getClass().getMethod(event_name);
         	set.invoke(ex);
    	}
    	catch(Exception e)
    	{
    		System.out.println("------------"+e);
    	}
	}
}



public class Event {
	
	static Map< String, Vector<String> > event_rule;
	private static ConnectionFactory factory ;
	private static Connection connection ;
	private static Channel channel ;
	public static String serviceclassName = "";
	public static String eventclassName = "";
	String msg = "";
	public static Vector<String> rules;
	public static Map< String, Vector<String> > services_map;
	PlatformAgent p ;
	protected  void process_event() {}
	
	public static void readServiceMap(String filePath, String fname)
	{
		services_map = new HashMap< String, Vector<String> >();
		File xmlFile = new File(filePath);
		File xmlF = new File(fname);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;

		try
		{
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);
			doc.getDocumentElement().normalize();

			NodeList classList = doc.getElementsByTagName("class-name");
			serviceclassName = classList.item(0).getTextContent().toString();

			NodeList serviceMetaData = doc.getElementsByTagName("Service");
			
			for (int i=0;i<serviceMetaData.getLength();i++)
			{
				NodeList serviceName = ((Element)serviceMetaData.item(i)).getElementsByTagName("Name");
				String name = serviceName.item(0).getTextContent().toString();

				Vector<String> metadata = new Vector<String>();
				String str;

				NodeList priority = ((Element)serviceMetaData.item(i)).getElementsByTagName("Priority");
				str = priority.item(0).getTextContent().toString();
				metadata.add(str);

				NodeList returnType = ((Element)serviceMetaData.item(i)).getElementsByTagName("Return-type");
				str = returnType.item(0).getTextContent().toString();
				metadata.add(str);

				NodeList parameterType = ((Element)serviceMetaData.item(i)).getElementsByTagName("Parameter-Type");
				
				for(int j=0; j<parameterType.getLength(); j++)
				{
					str = parameterType.item(j).getTextContent().toString();
					metadata.add(str);
				}
				services_map.put(name, metadata);
			}

			doc = dBuilder.parse(xmlF);
			doc.getDocumentElement().normalize();

			classList = doc.getElementsByTagName("class-name");
			eventclassName = classList.item(0).getTextContent().toString();
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	public Class<?> chktype(String typ) {
		
		switch(typ) {
			case "String":
				return String.class;
	
			case "int":
				return int.class;
			case "float":
				return float.class;
			case "double":
				return double.class;
		}
		return void.class;
		
	}
	protected void call_service(String service_name,Object ...allparams) throws IOException, TimeoutException
	{
		//String service_name = (String) allparams[0];
		try 
		{
			
			System.out.println("Service called = "+service_name);
	
			int priority = Integer.parseInt(services_map.get(service_name).elementAt(0));
			System.out.println("Priority = "+priority);
			Vector<String> vec = services_map.get(service_name);
			String types = "";
			String parameters = "";
			//Service S = new Service();
			if(priority<=5)
			{
				System.out.println("Invoking High priority service");
				Service s = new Service();
				
				//vec = services_map
				
				Class[] cl1=new Class[vec.size()-2];
				for(int i=2;i<vec.size();i++) {
					cl1[i-2]=chktype(vec.get(i));
					types = types + vec.get(i) + "[@{#|$]";
				}
				
				Method set = s.getClass().getMethod(service_name,cl1);
		        set.invoke(s,allparams); 	
				
			}
			else
			{
			
				System.out.println("Calling platform agent");
				
				for( Object s : allparams)
					parameters = parameters + s.toString() + "[@{#|$]";

				JSONObject obj = new JSONObject();
				obj.put("service_name", service_name);
				obj.put("types", types);
				obj.put("params",parameters);
				p = new PlatformAgent();
				p.init();
				p.callService(obj);
				System.out.println("after");
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	protected void call_workflow(String workflow_name,Object[] arr) throws IOException
	{
		JSONObject obj = new JSONObject();
		obj.put("service_name", workflow_name);
		obj.put("params", arr);
		channel.basicPublish("", "lb_incoming", null, obj.toJSONString().getBytes());
		
	}
	public void fun(String f)
	{
		msg = f;
	}
	public String fetch_rule_data(String rule_name) throws IOException, TimeoutException
	{
		/*Consumer consumer = new DefaultConsumer(channel) {
	      @Override
	       public void handleDelivery(String consumerTag, Envelope envelope,
	                                 AMQP.BasicProperties properties, byte[] body) throws IOException {
	        String message = new String(body, "UTF-8");
	    	 // msg = new String(body, "UTF-8");
	        
            System.out.println(rule_name);    
	        System.out.println(" [x] Received '" + message + "'");
	      }
	    };
		channel.basicConsume(rule_name, true, consumer);*/
		factory = new ConnectionFactory();
		factory.setHost("10.3.0.136");
		connection = factory.newConnection();
		channel = connection.createChannel();
		GetResponse response = channel.basicGet(rule_name, true);
		if (response != null) {
		    String message = new String(response.getBody(), "UTF-8");
		    fun(message);
		}
		System.out.println(msg);
		    return msg;
	}
	
	
	public static void main(String []args) throws Exception {
        // TODO code application logic here
		
		readServiceMap("services.xml", "event-rule-map.xml");
//		System.out.println("Services Map = "+services_map);
		// Event1 e = new Event1();

		Class eventClass = Class.forName(eventclassName);
		Object s = eventClass.newInstance();
    	try
    	{
    		Method[] allMethods = s.getClass().getDeclaredMethods();        
		    	
			for(int i = 0; i<10; i++) {	    		
			        for(Method m : allMethods)
					{
			        	Thread th = new Thread(new Events(m.getName()));
			     
			        	System.out.println(m.getName());
			        	th.start();
					}
			}
			
		 }
    	
    	catch(Exception ex)
    	{
    		System.out.println(ex);
    	}
    }
		
}

