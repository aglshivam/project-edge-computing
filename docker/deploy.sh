echo "Acquire::http::proxy \"http://proxy.iiit.ac.in:8080\";" | sudo tee /etc/apt/apt.conf
export http_proxy="http://proxy.iiit.ac.in:8080/"
export https_proxy="http://proxy.iiit.ac.in:8080/"
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable"
sudo apt-get update
sudo apt-get install -y docker-ce
sudo groupadd docker
sudo usermod -aG docker $USER
sudo mkdir -p /etc/systemd/system/docker.service.d
echo "[Service]" | sudo tee /etc/systemd/system/docker.service.d/http-proxy.conf
echo "Environment=\"HTTP_PROXY=http://proxy.iiit.ac.in:8080\"" | sudo tee -a /etc/systemd/system/docker.service.d/http-proxy.conf
sudo systemctl daemon-reload
sudo systemctl restart docker.service
sudo systemctl restart docker.socket
sudo docker run --net=host openjdk:7
sudo docker build -t test .
sudo docker run test
