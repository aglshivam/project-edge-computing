import java.io.IOException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.util.concurrent.TimeoutException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

public class DockerAgent {
	

	
	static void processRequest(String serviceQueueName) throws IOException, TimeoutException {
		ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        connectionFactory.setVirtualHost("/");
        connectionFactory.setHost("localhost");
        connectionFactory.setPort(5672);
        Connection connection = (Connection) connectionFactory.newConnection();
        Channel channel = ((com.rabbitmq.client.Connection) connection).createChannel();
        
        System.out.println("creating service queue name " + serviceQueueName);

        channel.queueDeclare(serviceQueueName, false, false, false, null);
        
        Consumer consumer = new DefaultConsumer(channel) {
        	int i = 0;
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                            throws IOException {
            	
            	String message = new String(body, "UTF-8");
            		
            	JSONParser parser = new JSONParser();
            	JSONObject jo = null;
            	try {
					jo = (JSONObject) parser.parse(message);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				};
				
				String serviceName = (String) jo.get("service_name");
				
				// Process service by calling the service method here
				
				Service s = new Service();
		    	try
		    	{

		    	Method set = s.getClass().getMethod(serviceName, String.class);
		         set.invoke(s, message);
		    	}
		    	catch(Exception e)
		    	{
		    		System.out.println(e);
		    	}
				
				
				
				System.out.println("Processing service " + serviceName + " for " + i + " times");
				i++;
				
            }
          };
          channel.basicConsume(serviceQueueName, true, consumer);
	}

	public static void main(String[] args) throws IOException, TimeoutException {

		String docker_id = args[0];
		processRequest(docker_id);

	}

}
