sudo apt-get remove -y docker docker-engine docker-ce docker.io
sudo apt-get purge -y docker-ce
sudo rm -rf /var/lib/docker
sudo rm /etc/systemd/system/docker.service.d/http-proxy.conf
sudo rm /etc/systemd/system/docker.service.d/https-proxy.conf