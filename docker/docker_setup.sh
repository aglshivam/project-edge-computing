sudo apt-get update
sudo apt-get install \
apt-transport-https \
ca-certificates \
curl \
software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce
echo "proxy setup"
sudo mkdir -p /etc/systemd/system/docker.service.d
sudo touch /etc/systemd/system/docker.service.d/http-proxy.conf
sudo touch /etc/systemd/system/docker.service.d/https-proxy.conf
sudo printf "[Service]\nEnvironment=\"HTTP_PROXY=http://proxy.iiit.ac.in:8080/\""\
" \n NO_PROXY=\"localhost, 127.0.0.1, iiit.ac.in, .iiit.ac.in, iiit.net,"\
" .iiit.net, 172.16.0.0/12, 192.168.0.0/16, 10.0.0.0/8\"" >> /etc/systemd/system/docker.service.d/http-proxy.conf 
sudo printf "[Service]\nEnvironment=\"HTTP_PROXY=http://proxy.iiit.ac.in:8080/\""\
" \n NO_PROXY=\"localhost, 127.0.0.1, iiit.ac.in, .iiit.ac.in, iiit.net,"\
" .iiit.net, 172.16.0.0/12, 192.168.0.0/16, 10.0.0.0/8\"" >> /etc/systemd/system/docker.service.d/https-proxy.conf
sudo systemctl daemon-reload
sudo systemctl restart docker
systemctl show --property=Environment docker
sudo docker build -t myimage .
sudo docker run myimage