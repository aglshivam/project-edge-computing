import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Send {

  private final static String QUEUE_NAME = "hello";

  public static void main(String[] argv) throws Exception {
    ConnectionFactory factory = new ConnectionFactory();
    factory.setUsername("guest");
    factory.setPassword("guest");
    factory.setVirtualHost("/");
   // connectionFactory.setHost(inetAddress.getHostAddress());
    factory.setPort(5672);

    factory.setHost("127.0.0.1");
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();

    
    // queueDeclare(java.lang.String queue, boolean passive, boolean durable, boolean exclusive, boolean autoDelete,
    //	java.util.Map<java.lang.String,java.lang.Object> arguments)
    channel.queueDeclare(QUEUE_NAME, false, false, false, null);
    String message = "Hello World!";
    
    // basicPublish(java.lang.String exchange, java.lang.String routingKey, AMQP.BasicProperties props, byte[] body)
    // Publish a message with both "mandatory" and "immediate" flags set to false
    channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
    System.out.println(" [x] Sent '" + message + "'");

    channel.close();
    connection.close();
  }
}