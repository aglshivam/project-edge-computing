import subprocess as sb
import sys
fname = sys.argv[1]
proc = sb.Popen(["cat /proc/meminfo | grep 'MemTotal'|awk '{print $2}'"], stdout=sb.PIPE, shell=True)
(out,err) = proc.communicate()
mem = int(out.strip())
proc = sb.Popen(["cat /proc/cpuinfo|grep -c 'processor'"], stdout=sb.PIPE, shell=True)
(out,err) = proc.communicate()
numpr = int(out.strip())
myIp = sys.argv[2]
expr = 'echo "%d,%d,%s" | cat >> %s'%(numpr,mem,myIp,fname)
proc = sb.Popen([expr], stdout=sb.PIPE, shell=True)
(out,err) = proc.communicate()
