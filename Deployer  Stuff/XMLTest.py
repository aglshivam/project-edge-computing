import xml.etree.cElementTree as ET

f = open("gateways.txt","r")
gateways = {}
for line in f:
	vals = line.split(',')
	gateways[vals[1]] = [vals[0],vals[2]]
f.close()

f = open("serverinfo.txt","r")
servers = {}
for line in f:
	vals = line.split(',')
	servers[vals[1]] = [vals[0],vals[2]]
loadBalancerIP = servers.keys()[0]
eventManagerIP = servers.keys()[1]
appServersIPs = servers.keys()[2:]

root = ET.Element("Systems")
gways = ET.SubElement(root, "Gateways")
for ip in gateways:
	gway = ET.SubElement(gways,"Gateway")
	ET.SubElement(gway,"Name").text = gateways[ip][0]
	ET.SubElement(gway,"IP").text = ip

lb = ET.SubElement(root,"Load-Balancer")
ET.SubElement(lb,"Name").text = servers[loadBalancerIP][0]
ET.SubElement(lb,"IP").text = loadBalancerIP

em = ET.SubElement(root,"Event-Manager")
ET.SubElement(em,"Name").text = servers[eventManagerIP][0]
ET.SubElement(em,"IP").text = eventManagerIP

apps = ET.SubElement(root, "Servers")
for ip in appServersIPs:
	app = ET.SubElement(apps,"Server")
	ET.SubElement(app,"Name").text = servers[ip][0]
	ET.SubElement(app,"IP").text = ip
	
tree = ET.ElementTree(root)
tree.write("configfile.xml")