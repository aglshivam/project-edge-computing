import subprocess as sb 
import xml.etree.cElementTree as ET
# Dependencies for Systems : sshpass and openssh-server (and start them using sudo service ssh start)
# servers.txt contains the information of all servers (ip,username,password)
# Format of the file : (csv file)
# Username,IP Address,Password
# gateways.txt contains the information of all gateways (ip,username,password)
# Format of the file : (csv file)
# Username, IP Address, Password
# Code Path to send to ~/Platform
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SEND CODE TO GATEWAYS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

f = open("gateways.txt","r")
gateways = {}
TO_GPATH = 'GatewayC'
GFILE_NAME = 'Gcode.zip'
for line in f:
	vals = line.split(',')
	gateways[vals[1]] = [vals[0],vals[2]]
f.close()
print "Gateways Found: ",gateways.keys()
for ip in gateways:
	name, syspass = gateways[ip]
	# Send the code using SCP
	cmd = 'sshpass -p \"'+syspass+'\" scp '+GFILE_NAME+' '+name+'@'+ip+':'+GFILE_NAME
	sb.call(cmd,shell=True)
	print "Gateway Code Sent to: ",name
	# Unzip the code at the Remote Machine using SSH
	cmd = 'sshpass -p \"'+syspass+'\" ssh '+name+'@'+ip+' unzip '+GFILE_NAME
	sb.call(cmd,shell=True)
	print "Gateway Code file Unzipped at: ",name
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ SEND CODE TO SERVERS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
f = open("serverinfo.txt","r")
# Dictionary of Servers
servers = {}
# TO Path
TO_CPATH = 'PlatformC/'
# Code Zip
CFILE_NAME = 'Pcode.zip'
for line in f:
	vals = line.split(',')
	servers[vals[1]] = [vals[0],vals[2]]

loadBalancerIP = servers.keys()[0]
eventManagerIP = servers.keys()[1]
serviceManagerIP = servers.keys()[2]
appServersIPs = servers.keys()[3:]

print "Load Balancer is:",loadBalancerIP
print "Event Manager is:",eventManagerIP
print "Service Manager is:",serviceManagerIP
"""
Code to run the Gateways
"""
print "Servers Read: ",servers.keys()

"""
GENERATE XML File
<Systems>
	<Gateways>
		<Gateway>
			<Name>
			<IP>
		</Gateway>
		<Gateway>
			<Name>
			<IP>
		</Gateway>
	</Gateways>
	<Load-Balancer>
		<Name>
		<IP>
	</Load-Balancer>
	<Event-Manager>
		<Name>
		<IP>
	</Event-Manager>
	<Service-Manager>
		<Name>
		<IP>
	</Service-Manager>
	<Server-Manager>
		<Name>
		<IP>
	</Server-Manager>
	<Servers>
		<Server>
			<IP>
			<Name>
		</Server>
		<Server>
			<IP>
			<Name>
		</Server>
	</Servers>
</Systems>
"""
# Create the XML Config File
root = ET.Element("Systems")
gways = ET.SubElement(root, "Gateways")
for ip in gateways:
	gway = ET.SubElement(gways,"Gateway")
	ET.SubElement(gway,"Name").text = gateways[ip][0]
	ET.SubElement(gway,"IP").text = ip

lb = ET.SubElement(root,"Load-Balancer")
ET.SubElement(lb,"Name").text = servers[loadBalancerIP][0]
ET.SubElement(lb,"IP").text = loadBalancerIP

em = ET.SubElement(root,"Event-Manager")
ET.SubElement(em,"Name").text = servers[eventManagerIP][0]
ET.SubElement(em,"IP").text = eventManagerIP

sm = ET.SubElement(root,"Service-Manager")
ET.SubElement(sm,"Name").text = servers[serviceManagerIP][0]
ET.SubElement(sm,"IP").text = serviceManagerIP

srm = ET.SubElement(root,"Server-Manager")
ET.SubElement(srm,"Name").text = servers[serviceManagerIP][0]
ET.SubElement(srm,"IP").text = serviceManagerIP

apps = ET.SubElement(root, "Servers")
for ip in appServersIPs:
	app = ET.SubElement(apps,"Server")
	ET.SubElement(app,"Name").text = servers[ip][0]
	ET.SubElement(app,"IP").text = ip
	

tree = ET.ElementTree(root)
tree.write("configfile.xml")

ConFILE_NAME = 'configfile.xml'

# Sending the code to the machines and unzipping them

for ip in servers:
	name, syspass = servers[ip]
	# Send the config file
	cmd = 'sshpass -p \"'+syspass+'\" scp '+ConFILE_NAME+' '+name+'@'+ip+':'+ConFILE_NAME
	sb.call(cmd,shell=True)
	# Send the code using SCP
	cmd = 'sshpass -p \"'+syspass+'\" scp '+CFILE_NAME+' '+name+'@'+ip+':'+CFILE_NAME
	sb.call(cmd,shell=True)
	print "Code Sent to: ",name
	# Unzip the code at the Remote Machine using SSH
	cmd = 'sshpass -p \"'+syspass+'\" ssh '+name+'@'+ip+' unzip '+CFILE_NAME
	sb.call(cmd,shell=True)
	print "Code file Unzipped at: ",name

# Starting the Load Balancer

LFILE_NAME = "LoadBalancer.java"
sName, sPass = servers[loadBalancerIP]
cmd = 'sshpass -p \"'+syspass+'\" scp '+ConFILE_NAME+' '+sName+'@'+loadBalancerIP+':'+ConFILE_NAME
sb.call(cmd,shell=True)
cmd = 'sshpass -p \"'+sPass+'\" ssh '+sName+'@'+loadBalancerIP+' cd '+TO_CPATH+';export CP = export CP=.:amqp-client-4.0.2.jar:slf4j-api-1.7.21.jar:slf4j-simple-1.7.22.jar:json-simple-1.1.1.jar;javac -cp $CP '+LFILE_NAME+';nohup java -cp $CP '+LFILE_NAME.split('.')[0]+loadBalancerIP+' '+lbAnalyserIP+' &'
sb.call(cmd,shell=True)

# Starting the Server and Service Manager

SMFILE_NAME = "ServiceManager.java"
SRMFILE_NAME = "ServerManager.java"
sName, sPass = servers[serviceManagerIP]
cmd = 'sshpass -p \"'+syspass+'\" scp '+ConFILE_NAME+' '+sName+'@'+serviceManagerIP+':'+ConFILE_NAME
sb.call(cmd,shell=True)
cmd = 'sshpass -p \"'+sPass+'\" ssh '+sName+'@'+serviceManagerIP+' cd '+TO_CPATH+';javac -cp $CP = export CP=.:amqp-client-4.0.2.jar:slf4j-api-1.7.21.jar:slf4j-simple-1.7.22.jar:json-simple-1.1.1.jar'+SMFILE_NAME+';nohup java -cp $CP '+SMFILE_NAME.split('.')[0]+' &'
sb.call(cmd,shell=True)
cmd = 'sshpass -p \"'+sPass+'\" ssh '+sName+'@'+serviceManagerIP+' cd '+TO_CPATH+';javac -cp $CP = export CP=.:amqp-client-4.0.2.jar:slf4j-api-1.7.21.jar:slf4j-simple-1.7.22.jar:json-simple-1.1.1.jar'+SRMFILE_NAME+';nohup java -cp $CP '+SRMFILE_NAME.split('.')[0]+' &'
sb.call(cmd,shell=True)

# Starting the Event Manager

EFILE_NAME = "eventManager.java"
sName, sPass = servers[eventManagerIP]
cmd = 'sshpass -p \"'+syspass+'\" scp '+ConFILE_NAME+' '+sName+'@'+eventManagerIP+':'+ConFILE_NAME
sb.call(cmd,shell=True)
cmd = 'sshpass -p \"'+sPass+'\" ssh '+sName+'@'+eventManagerIP+' cd '+TO_CPATH+';javac -cp $CP = export CP=.:amqp-client-4.0.2.jar:slf4j-api-1.7.21.jar:slf4j-simple-1.7.22.jar:json-simple-1.1.1.jar'+EFILE_NAME+';nohup java -cp $CP '+EFILE_NAME.split('.')[0]+' &'
sb.call(cmd,shell=True)


print "Remaining App Servers are:",appServersIPs

# Sending Event Manager Code

EFILE_NAME = 'Ecode.zip'
emName, emPass = servers[eventManagerIP]

sb.call(cmd,shell=True)
cmd = 'sshpass -p \"'+emPass+'\" scp '+EFILE_NAME+' '+emName+'@'+eventManagerIP+':'+EFILE_NAME
sb.call(cmd,shell=True)
print "Code Sent to Event Manager:",emName
cmd = 'sshpass -p \"'+emPass+'\" ssh '+emName+'@'+eventManagerIP+' unzip '+EFILE_NAME
sb.call(cmd,shell=True)
print "Code file Unzipped at: ",emName
"""
Code to start the Event Manager
"""